#!/usr/bin/env bash

# Source this file to use the pretty_print($1 $2) function

# Pretty-print function that will take a message and an exit code as arguments.
# It will output the message and an [OK] or [FAIL] at the end of the line,
# indicating a status for a command executed.
#
# $1 - The message to print
# $2 - The exit code of the command executed
pretty_print() {
  local msg="$1"
  local status=$2
  local status_msg
  local color
  local color_reset

  color_reset=$(tput sgr0)

  local pad_limit
  pad_limit=$(tput cols)

  local pad
  pad=$(printf "%*s" "$pad_limit")
  pad=${pad// /.}

  if (( status == 0 )); then
    status_msg="[OK]"
    color=$(tput setaf 2)
  else
    status_msg="[FAIL]"
    color=$(tput setaf 1)
  fi

  printf '%s ' "$msg"
  printf '%*.*s' 0 $((pad_limit - ${#msg} - ${#status_msg} -2 )) "$pad"
  printf ' %s\n' "${color}$status_msg${color_reset}"
  status_msg=${status_msg:1}
}
