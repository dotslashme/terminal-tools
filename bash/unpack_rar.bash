#!/usr/bin/env bash

shopt -s globstar nullglob

usage() {
	local command="$1"
	printf "%s [absolute path to root of media library]\n" "${command}"
}

unpack() {
	local file="$1"
	local path="${file%/*}"
	unrar e -o- -inul "${file}" "${path}/"
}

main() {
	local rar_files=("$1/**/*.rar")

	for f in ${rar_files[@]}; do
		unpack "${f}"
	done	
}


if [[ $# -lt 1 ]]; then
	usage "$0"
	exit 1
elif ! command -v unrar &> /dev/null; then
	printf "The binary 'unrar' is not installed - Exiting!\n"
	exit 1
else
	main "$1"
fi
